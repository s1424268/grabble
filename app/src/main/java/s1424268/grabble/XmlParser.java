package s1424268.grabble;


import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rosss on 19/11/2016.
 */

public class XmlParser {
    private static final String LOG = ("Parser");
    private static final String ns = null;
    public List parse(InputStream in) throws XmlPullParserException, IOException{
        try{
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in,null);
            parser.nextTag();
            return readFeed(parser);
        }  finally {
            in.close();
        }
    }
    private List readFeed(XmlPullParser parser) throws XmlPullParserException, IOException{
        List entries = new ArrayList();
        parser.require(XmlPullParser.START_TAG, ns,"kml");
        while (parser.next()!=XmlPullParser.END_TAG){
            if(parser.getEventType()!=XmlPullParser.START_TAG){
                continue;
            }
            String name = parser.getName();
            if(name.equals("Placemark")){
                Log.i(LOG, "readFeed");
                entries.add(readPlacemark(parser));
            }else{
                skip(parser);
            }
        }
        return entries;
    }
    public static class Placemark {
        public final String name;
        public final String description;
        public final String[] coords;
        private Placemark(String name, String description, String[] coords){
            Log.i(LOG, "placemark made");
            this.name = name;
            this.description = description;
            this.coords = coords;
        }
    }


    private Placemark readPlacemark(XmlPullParser parser)throws XmlPullParserException,IOException{
        parser.require(XmlPullParser.START_TAG, ns, "Placemark");
        String name = null; String description = null; String[] coords = new String[2];
        while (parser.next()!= XmlPullParser.END_TAG){
            if(parser.getEventType()!=XmlPullParser.START_TAG)
                continue;
            String pName = parser.getName();
            if(pName.equals("name")){
                name = readName(parser);
            }else if(pName.equals("description")){
                description = readDescription(parser);
            }else if(pName.equals("Point")){
                coords = readCoordinates(parser);
            } else{
                Log.i(LOG, "error");
                skip(parser);
            }
        }
        return new Placemark(name, description, coords);
    }
    private String readName(XmlPullParser parser) throws IOException,XmlPullParserException{
        parser.require(XmlPullParser.START_TAG, ns, "name");
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "name");
        return title;
    }
    private String[] readCoordinates(XmlPullParser parser) throws IOException, XmlPullParserException {
        Log.i(LOG, " before next "+ parser.getName());
        parser.nextTag();
        Log.i(LOG,"after next "+parser.getName());
        Log.i(LOG,"coordinates reading ");
        String latitude;
        String longitude;
        parser.require(XmlPullParser.START_TAG, ns, "coordinates");
        String coordinates = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "coordinates");
        latitude = coordinates.split(",")[1];
        longitude = coordinates.split(",")[0];
        parser.nextTag();
        return new String[]{latitude, longitude};
    }
    private String readDescription(XmlPullParser parser) throws IOException, XmlPullParserException{
        parser.require(XmlPullParser.START_TAG, ns, "description");
        String summary = readText(parser);
        parser.require(XmlPullParser.END_TAG,ns,"description");
        return summary;
    }
    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException{
        String result = "";
        if(parser.next() == XmlPullParser.TEXT){
            result = parser.getText();
            parser.nextTag();
        }
        Log.i(LOG,"read text is: " + result);
        return result;
    }
    private void skip(XmlPullParser parser) throws IOException, XmlPullParserException{
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
 }

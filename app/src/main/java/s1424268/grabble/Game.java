package s1424268.grabble;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.vision.text.Text;

import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class Game extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, GoogleMap.OnMarkerClickListener, View.OnClickListener {

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest locationRequest;
    private List<Marker> markers = new ArrayList<Marker>();
    private Marker currentLocation;
    private List<String> inventory = new ArrayList<String>();
    private int userScore;
    private String username;
    private String wod;
    private String[] dictionary = new String[23869];
    private boolean doubleBackToExitPressedOnce;
    private int pickCount;


    private String LOG = "Grabble";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(LOG,"on Create");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        TextView txtScore = (TextView) findViewById(R.id.txt_userScore);
        txtScore.setText("Score: 0");
        if (getIntent().getStringExtra("username") !=null){
            username = getIntent().getStringExtra("username");
        }
        pickCount = 0;
        doubleBackToExitPressedOnce = false;
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        new downloadURL().execute("http://www.inf.ed.ac.uk/teaching/courses/selp/coursework/grabble.txt");
        new DownloadXmlTask().execute(getCorrectURL());

        Button inventory = (Button) findViewById(R.id.btn_inventory);
        inventory.setOnClickListener(this);
    }
    private String getWordOfDay(SharedPreferences date){
        Calendar cal = Calendar.getInstance();
        int d = date.getInt("date", 400);   //if there is no day held in the preferences, set variable to 400(a day that is not valid)
        SharedPreferences.Editor editor = date.edit();
        Log.i(LOG, "date is " + d + "current day is " + cal.get(Calendar.DAY_OF_YEAR));
        if (d == 400 || !(d ==(cal.get(Calendar.DAY_OF_YEAR)))){
            Log.i(LOG, "changing date");
            editor.putInt("date",cal.get(Calendar.DAY_OF_YEAR)); //if there is no day in preferences then set it to the current one
            editor.putString("word_of_day", setWord());
            editor.commit();

        }
        return date.getString("word_of_day", "");
    }
    private String setWord(){ //function that returns a word of the day.
        Log.i(LOG, "changing word");
        Random rn = new Random();
        return dictionary[rn.nextInt(23869)];
    }

    public class downloadURL extends AsyncTask<String, Void, String[]> {

        @Override
        protected String[] doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                String line;
                for (int i = 0; i < 23869; i++) {
                    dictionary[i] = in.readLine();
                }
                in.close();
                return dictionary;
            } catch (IOException e) {
                Log.i(LOG, "IO Exception");
                return null;
            }
        }

        @Override
        protected void onPostExecute(String[] result) {
            Log.i(LOG, "downloadURL complete");
            SharedPreferences date = getApplicationContext().getSharedPreferences("date", 0);
            wod = "";
            TextView wordOfDay = (TextView) findViewById(R.id.txt_wordOfDay);
            wod = getWordOfDay(date);
            wordOfDay.setText("Word of day: " + wod);


        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMaxZoomPreference((float)25);
        mMap.setMinZoomPreference((float)16.5);
        LatLngBounds bounds = new LatLngBounds(new LatLng(55.942617, -3.192473),new LatLng(55.946233, -3.184319) );
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bounds.getCenter(), (float)17.5));
        mMap.setLatLngBoundsForCameraTarget(bounds);
        mMap.setOnMarkerClickListener(this);


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, 3);
            return;
        }
        Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (lastLocation != null){
            LatLng last = new LatLng(lastLocation.getLatitude(),lastLocation.getLongitude());
            currentLocation = mMap.addMarker(new MarkerOptions().position(last).title("Current Position").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

        }
        locationRequest = new LocationRequest();
        locationRequest.setInterval(5 * 1000);
        locationRequest.setFastestInterval(2 * 1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        requestLocationUpdates();

    }

    public void requestLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, 3);
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        switch(requestCode){
            case 3: {
                if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

                }else{

                }
                return;
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (currentLocation != null){
           currentLocation.remove();
        }
        LatLng newLoc = new LatLng(location.getLatitude(), location.getLongitude());
        currentLocation = mMap.addMarker(new MarkerOptions().position(newLoc).title("current position").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

    }
    public boolean checkPlacemarkProximity(Marker marker){ //method checks user's distance to placemark that has been clicked
        Location currLocation = new Location("");
        currLocation.setLongitude(currentLocation.getPosition().longitude);
        currLocation.setLatitude(currentLocation.getPosition().latitude);
        Location markerLocation = new Location("");
        markerLocation.setLongitude(marker.getPosition().longitude);
        markerLocation.setLatitude(marker.getPosition().latitude);
        if (calcDistance(currLocation, markerLocation)<10){
            return true;
        }
        return false;
    }
    public float calcDistance(Location markerLoc, Location currLocation){
        return (currLocation.distanceTo(markerLoc));
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        if (markers.contains(marker)){
            if(checkPlacemarkProximity(marker)){
                pickCount++;
                if (pickCount<=20) {
                    inventory.add(marker.getTitle());
                    markers.remove(marker);
                    marker.remove();
                }else{
                    String text = "Inventory full, remove letters to pick up more";
                    Toast toast = Toast.makeText(context, text,duration);
                    toast.show();
                }

            }else{
                String text = "Letter is too far away!";
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() ==  R.id.btn_inventory){
            Bundle test = new Bundle();
            test.putSerializable("ARRAYLIST",(Serializable)inventory);
            test.putString("WOD", wod);
            Intent i = new Intent(this, Inventory.class);
            i.putExtra("BUNDLE", test);
            startActivityForResult(i, 2);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) throws NullPointerException{
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 2){
            if (resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();
                userScore += data.getIntExtra("score", RESULT_OK);
                TextView v = (TextView) findViewById(R.id.txt_userScore);
                v.setText("Score: " + userScore);
                if (extras.containsKey("word")) {
                    Log.i(LOG, "fetched " + data.getStringExtra("word"));
                    removeLetters(data.getStringExtra("word"));
                    if (extras.containsKey("word2")) {
                        removeLetters(data.getStringExtra("word2"));
                    }
                    if(data.getStringExtra("word") == wod){

                    }
                }
                if (extras.containsKey("remove")){
                    pickCount = (20 - data.getStringExtra("remove").length());
                    removeLetters(data.getStringExtra("remove"));
                }

            }
        }
    }
    private void removeLetters(String word){
        for(char c : word.toCharArray()){
            inventory.remove(Character.toString(c));
        }
    }

    private class DownloadXmlTask extends AsyncTask<String, Void, List<XmlParser.Placemark>>{
        @Override
        protected List<XmlParser.Placemark> doInBackground(String...urls){
            try{
                return loadXmlFromNetwork(urls[0]);
            } catch(IOException e){
                Log.e(LOG,"IOException");
                return null;
            }catch(XmlPullParserException e){
                Log.e(LOG,"XmlPullParserException");
                return null;
            }
        }
        @Override
        protected void onPostExecute(List<XmlParser.Placemark> result){
            setMarkers(result);
        }
    }
    private void setMarkers(List<XmlParser.Placemark> placemarks){ //method takes list of placemarks created from parser and adds them to the map.
        for (XmlParser.Placemark placemark : placemarks){
            Location placemark_loc = new Location("");
            placemark_loc.setLatitude(Float.parseFloat(placemark.coords[0]));
            placemark_loc.setLongitude(Float.parseFloat(placemark.coords[1]));
            markers.add(mMap.addMarker(new MarkerOptions().position(new LatLng(placemark_loc.getLatitude(),placemark_loc.getLongitude()))
                    .title(placemark.description)
                    .icon(BitmapDescriptorFactory.fromResource(getResources().getIdentifier(placemark.description.toLowerCase() + "_small", "drawable", getPackageName())))));
        }
    }


    private List loadXmlFromNetwork(String urlString) throws IOException, XmlPullParserException{
        InputStream stream = null;
        XmlParser xmlParser = new XmlParser();
        List<XmlParser.Placemark> placemarks = null;
        String name = null;
        String description = null;
        String[] coords = null;
        try {
            stream = downloadUrl(urlString);
            placemarks = xmlParser.parse(stream);
        }finally{
            if(stream != null){
                stream.close();
            }
        }
        return placemarks;
    }

    private InputStream downloadUrl(String urlString) throws IOException{
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10000);
        conn.setConnectTimeout(15000);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        conn.connect();
        return conn.getInputStream();
    }
    private String getCorrectURL(){ //method to form and return the correct url string depending on which day of the week the user is playing.
        Calendar calendar = Calendar.getInstance();
        String d = "";
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        switch(day){
            case Calendar.MONDAY:
                d = "monday";
                break;
            case Calendar.TUESDAY:
                d = "tuesday";
                break;
            case Calendar.WEDNESDAY:
                d = "wednesday";
                break;
            case Calendar.THURSDAY:
                d = "thursday";
                break;
            case Calendar.FRIDAY:
                d = "friday";
                break;
            case Calendar.SATURDAY:
                d = "saturday";
                break;
            case Calendar.SUNDAY:
                d = "sunday";
                break;
        }
        return "http://www.inf.ed.ac.uk/teaching/courses/selp/coursework/" + d + ".kml";


    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.i(LOG,"destroy");
    }
    @Override
    public void onResume(){
        super.onResume();
        Log.i(LOG, "resume");
    }
    public void onPause(){
        super.onPause();
        Log.i(LOG, "pause");
    }
    @Override
    public void onBackPressed() {   //http://stackoverflow.com/questions/8430805/clicking-the-back-button-twice-to-exit-an-activity
        if (doubleBackToExitPressedOnce) {
            SharedPreferences scorePref = getApplicationContext().getSharedPreferences(username, MODE_PRIVATE);
            Log.i(LOG, "fetching score " + scorePref.getInt(username, 0));
            int userTotalScore = scorePref.getInt(username, 0);
            userTotalScore = userTotalScore + userScore;
            SharedPreferences.Editor editor = scorePref.edit();
            editor.putInt(username, userTotalScore);
            editor.commit();
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}

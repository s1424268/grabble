package s1424268.grabble;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

public class Inventory extends AppCompatActivity implements View.OnClickListener {
    private GridLayout gridLayout;
    private static final String LOG = "Inventory";
    private List<String> markers;
    List<Button> btns = new ArrayList<Button>();
    private int wordState;
    private Button btn_undo;
    private Button btn_submit;
    private Button btn_remove;
    private Stack<Button> selectionHist = new Stack<Button>();
    private String[] dictionary = new String[23869];
    private HashMap<String, Integer> letterScores = new HashMap<String, Integer>();
    private int userScore;
    private String wod;
    private String lettersToRemove;
    private Intent activityResult = new Intent();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(LOG, "in activity");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory);
        gridLayout = (GridLayout) findViewById(R.id.inventoryLayout);
        gridLayout.setColumnCount(4);
        gridLayout.setRowCount(5);
        wordState = 0;
        userScore = 0;
        lettersToRemove = "";
        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
        btn_undo = (Button) findViewById(R.id.btn_Undo);
        btn_undo.setOnClickListener(this);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(this);
        btn_remove = (Button) findViewById(R.id.btn_remove);
        btn_remove.setOnClickListener(this);
        Log.i(LOG, "bundle made");
        wod = args.getString("WOD");
        markers = (ArrayList<String>) args.getSerializable("ARRAYLIST");
        new downloadURL().execute("http://www.inf.ed.ac.uk/teaching/courses/selp/coursework/grabble.txt"); //start Async task to extract words from txt file held at url.
        createLetterMapping();
        populateInventory();
    }

    private void createLetterMapping() {
        letterScores.put("A", 3);
        letterScores.put("B", 20);
        letterScores.put("C", 13);
        letterScores.put("D", 10);
        letterScores.put("E", 1);
        letterScores.put("F", 15);
        letterScores.put("G", 18);
        letterScores.put("H", 9);
        letterScores.put("I", 5);
        letterScores.put("J", 25);
        letterScores.put("K", 22);
        letterScores.put("L", 11);
        letterScores.put("M", 14);
        letterScores.put("N", 6);
        letterScores.put("O", 4);
        letterScores.put("P", 19);
        letterScores.put("Q", 24);
        letterScores.put("R", 8);
        letterScores.put("S", 7);
        letterScores.put("T", 2);
        letterScores.put("U", 12);
        letterScores.put("V", 21);
        letterScores.put("W", 17);
        letterScores.put("X", 23);
        letterScores.put("Y", 16);
        letterScores.put("Z", 26);
    }

    private void populateInventory() { //method to display the letters the user has collected
        int i = 0;
        for (String m : markers) {
            Button btn = new Button(this);
            int id = getResources().getIdentifier("btn_inv" + (i + 1), "id", this.getPackageName());
            btn.setId(id);
            btn.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            btn.setText(m);;
            btn.setOnClickListener(this);
            btns.add(btn);
            gridLayout.addView(btn);
            i++;
        }

    }

    @Override
    public void onClick(View v) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        if (v.getId() == R.id.btn_Undo) { //executes when undo button is clicked
            if (wordState > 0) {
                updateState(-1);
                selectionHist.pop().setVisibility(View.VISIBLE);
                selectView().setText("");
            } else {   //https://developer.android.com/guide/topics/ui/notifiers/toasts.html
                CharSequence text = "No moves to undo!";
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        }
        if (v.getId() == R.id.btn_submit) {  //executes when submit button is clicked
            if (wordState == 7) {
                formWord();
            } else {
                CharSequence text = "Words must have seven letters!";
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }

        }
        if (v.getId() == R.id.btn_remove){
            Log.i(LOG, "removing");
            String letter = "";
            int iterations = selectionHist.size();
            for (int i = 0; i < iterations; i++){
                Log.i(LOG, "in loop");
                letter = (String)selectionHist.pop().getText();
                markers.remove(letter);
                lettersToRemove += letter;
                updateState(-1);
                selectView().setText("");

            }
        }
        for (Button b : btns) {  //executes when one of inventory buttons is clicked
            if (v.getId() == b.getId()) {
                if ((wordState < 7)) {
                    Log.i(LOG, "got match");
                    b.setVisibility(View.INVISIBLE);
                    selectionHist.push(b);
                    String letter = b.getText().toString();
                    selectView().setText(letter);
                    updateState(1);
                } else {
                    CharSequence text = "Only seven letter words may be formed!";
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
            }
        }
    }

    private void formWord() {   //method forms a string of the word the user has submited
        String word = "";
        Button[] letters = new Button[7];
        int wordValue = 0;
        for (int i = 0; i < 7; i++) {
            letters[i] = selectionHist.pop();
            wordValue = wordValue + letterScores.get((String) letters[i].getText());
            word += letters[i].getText();
            updateState(-1);
            selectView().setText("");
        }

        StringBuilder strb = new StringBuilder(); //reverse order of word
        strb.append(word);
        strb = strb.reverse();
        word = strb.toString();
        activityResult.putExtra("word", word);
        if (word .equals(wod.toUpperCase())){
            wordValue = wordValue * 2;
        }
        if (!isWord(word)) { //return buttons to inventory if word is invalid
            for (Button b : letters) {
                b.setVisibility(View.VISIBLE);
            }
        } else { //if a valid word is submitted then remove letters from inventory.
            if (activityResult.hasExtra("word")){
                activityResult.putExtra("word2", word);
            }else {
                activityResult.putExtra("word", word);
            }
            for (Button b : letters){
                markers.remove(b.getText());
            }
            updateScore(wordValue);
        }
    }

    private Boolean isWord(String word) { //checks the submitted word against Grabble dictionary returns true for a match, false otheriwse
        for (String w : dictionary) {
            if (w.toUpperCase().equals(word)) {
                return true;
            }
        }
        Log.i(LOG, "word is wrong");
        return false;
    }

    private void updateScore(int score) {
        userScore = userScore + score;
        TextView v = (TextView) findViewById(R.id.txt_score);
        v.setText("Score is: " + userScore);
    }

    public class downloadURL extends AsyncTask<String, Void, String[]> {

        @Override
        protected String[] doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                String line;
                for (int i = 0; i < 23869; i++) {
                    dictionary[i] = in.readLine();
                }
                in.close();
                return dictionary;
            } catch (IOException e) {
                Log.i(LOG, "IO Exception");
                return null;
            }
        }

        @Override
        protected void onPostExecute(String[] result) {
            Log.i(LOG, "downloadURL complete");
        }

    }

    private TextView selectView() {  //method to select which of the 7 letters the user is currently picking.
        TextView t = new TextView(this);
        switch (wordState) {
            case 0:
                t = (TextView) findViewById(R.id.txt_let1);
                break;
            case 1:
                t = (TextView) findViewById(R.id.txt_let2);
                break;
            case 2:
                t = (TextView) findViewById(R.id.txt_let3);
                break;
            case 3:
                t = (TextView) findViewById(R.id.txt_let4);
                break;
            case 4:
                t = (TextView) findViewById(R.id.txt_let5);
                break;
            case 5:
                t = (TextView) findViewById(R.id.txt_let6);
                break;
            case 6:
                t = (TextView) findViewById(R.id.txt_let7);
        }
        return t;
    }

    private void updateState(int change) {
        wordState = wordState + change;
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(LOG, "paused");
    }
    @Override
    public void onStop(){
        super.onStop();
        Log.i(LOG, "stopped");
    }
    @Override
    public void onResume(){
        super.onResume();
        Log.i(LOG, "resumed ");
    }
    @Override
    public void onStart(){
        super.onStart();
        Log.i(LOG, "start");
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.i(LOG, "destroy");

    }
    @Override
    public void onRestart(){
        super.onRestart();
        Log.i(LOG,"Restart");
    }
    @Override
    public void onBackPressed() {
        activityResult.putExtra("score", userScore);
        if(lettersToRemove.length() > 0){
            activityResult.putExtra("remove", lettersToRemove);
        }
        setResult(RESULT_OK, activityResult);
        super.onBackPressed();
    }
}



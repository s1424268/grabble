package s1424268.grabble;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends Activity implements View.OnClickListener{
    private Button btn_login;
    private Button btn_Register;
    private EditText et_username;
    private EditText et_password;
    private TextView currentUser;
    private String username;
    private Intent intent;
    SharedPreferences pref;
    private static final String LOG = "Login";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        pref = getApplicationContext().getSharedPreferences("userDetails", 0);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_glogin);
        btn_Register = (Button) findViewById(R.id.btn_Register);
        btn_Register.setOnClickListener(this);
        et_password = (EditText) findViewById(R.id.et_password);
        et_username = (EditText) findViewById(R.id.et_username);
        btn_login = (Button) findViewById(R.id.btn_Login);
        btn_login.setOnClickListener(this);
        currentUser = (TextView) findViewById(R.id.txt_currentUser);
        if (getIntent().getStringExtra("username") !=null){
            currentUser.setText("Current User: " + getIntent().getStringExtra("username"));
        }

        intent = new Intent();
    }

    @Override
    public void onClick(View v) {
        username = et_username.getText().toString();
        String password = et_password.getText().toString();
        Context context = getApplicationContext();

        int duration = Toast.LENGTH_SHORT;
        if (v.getId() == R.id.btn_Register){    //when the register button is clicked check if the username is already in use, if it isn't then register the username/password
            SharedPreferences.Editor editor = pref.edit();
            if(pref.getString(username, null) == null){
                editor.putString(username,password);
                editor.commit();
                currentUser.setText("Current User: " + username);
                String text = "Register succesful";
                intent.putExtra("username", username);
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }else{
                String text = "Username already exists";
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        }
        if (v.getId() == R.id.btn_Login){   //when the login button is clicked check if the password associated with the registered username matches the inputed password.
            if((pref.getString(username, null) != null) && pref.getString(username, null).equals(password)){
                String text = "Login succesful";
                currentUser.setText("Current User: " + username);
                intent.putExtra("username", username);
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }else{
                String text = "password incorrect/user does not exist";
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        }
    }


    @Override
    public void onBackPressed(){
        if (intent.hasExtra("username")){
            setResult(RESULT_OK, intent);
        }
        super.onBackPressed();
    }
}
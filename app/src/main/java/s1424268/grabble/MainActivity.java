package s1424268.grabble;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

public class MainActivity extends Activity implements View.OnClickListener{
    private String text;
    private String LOG = "MainActivity";
    private String username;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn_Login = (Button) findViewById(R.id.btn_Login);
        btn_Login.setOnClickListener(this);
        Button btn_Start = (Button) findViewById(R.id.btn_Start);
        btn_Start.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_Login:
                Log.i(LOG, "login pressed");
                Intent login = new Intent(this, LoginActivity.class);
                login.putExtra("username", username);
                startActivityForResult(login, 1);
                break;
            case R.id.btn_Start:
                Intent game = new Intent(this, Game.class);
                game.putExtra("username", username);
                Log.i(LOG, "sent username " + username);
                startActivity(game);
                break;
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == 1) && (resultCode == RESULT_OK)){
            username = data.getStringExtra("username");
        }
    }
}
